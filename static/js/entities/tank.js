'use strict';

// the main game object - the tank

class Tank {
  constructor(x, y) {
    this.body = sprite("image-tank-body");
    this.body.anchor.set(0.5);
    STATE.world.addChild(this.body);

    this.tower = sprite("image-tank-tower");
    this.tower.anchor.set(0.5, 0.25);
    STATE.world.addChild(this.tower);

    this.setPosition(x, y);
    this.setAccelerator(false);
    this.setReverse(false);
    this.setVelocity(0.0);
    this.maxVelocity = 3.0;
    this.minVelocity = -2.0;
    this.bodyTurnSpeed = 0.02;
    this.towerTurnSpeed = 0.05;
  }

  setPosition(x, y) {
    this.x = x;
    this.y = y;
    this.body.position.set(x, y);
    this.tower.position.set(x, y);
  }

  getX() { return this.x; }
  getY() { return this.y; }

  setBodyRotation(radians) {
    this.body.rotation = radians;
  }

  setTowerRotation(radians) {
    this.tower.rotation = radians;
  }

  setAccelerator(state) {
    this.accelerator = state;
  }

  setReverse(state) {
    this.reverse = state;
  }

  setVelocity(value) {
    this.velocity = value;
  }

  setTurningLeft(value) {
    this.bodyTurningLeft = value;
  }

  setTurningRight(value) {
    this.bodyTurningRight = value;
  }

  setTowerTarget(x, y) {
    this.towerTargetX = x;
    this.towerTargetY = y;
  }

  update(delta) {
    // turn
    if (this.bodyTurningLeft) {
      this.body.rotation = this.body.rotation - this.bodyTurnSpeed * delta;
    } else if (this.bodyTurningRight) {
      this.body.rotation = this.body.rotation + this.bodyTurnSpeed * delta;
    }
    let dx = this.towerTargetX - this.x;
    let dy = this.towerTargetY - this.y;
    let towerTargetAngle = Math.atan2(dy, dx) - PI05;
    let da = minAngleDelta(this.tower.rotation, towerTargetAngle);
    if (da > 0.0) {
      this.tower.rotation = (this.tower.rotation + Math.min(da * 0.09, this.towerTurnSpeed)) % PI2;
    } else if (da < 0.0) {
      this.tower.rotation = (this.tower.rotation - Math.min(-da * 0.09, this.towerTurnSpeed)) % PI2;
    }

    // move
    if (this.accelerator) {
      this.velocity = this.velocity + (this.maxVelocity - this.velocity) / 2.0;
    } else if (this.reverse) {
      this.velocity = this.velocity + (this.minVelocity - this.velocity) / 2.0;
    } else {
      this.velocity = this.velocity * 0.9;
    }
    let vx = Math.cos(this.body.rotation + PI05) * this.velocity;
    let vy = Math.sin(this.body.rotation + PI05) * this.velocity;
    this.setPosition(this.x + vx * delta, this.y + vy * delta);
  }

  dispose() {
    STATE.world.removeChild(this.body);
    STATE.world.removeChild(this.tower);
  }
}
