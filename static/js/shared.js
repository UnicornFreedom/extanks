'use strict';

// everything in here is shared between all JS files

// constants
//--------------------------------------------------------------------------------------------------------------------//
const PI05 = Math.PI * 0.5;
const PI = Math.PI;
const PI2 = Math.PI * 2.0;

const Z_GROUND = -1;
const Z_NORMAL = 0;
const Z_AIR = 1;

const LOADER = PIXI.Loader.shared;

// game state
//--------------------------------------------------------------------------------------------------------------------//
let STATE = {
  // set to `true` when all resources are loaded, game objects constructed, i.e. the game is ready to be played
  loaded: false,
  // `x` and `y` is the position the camera *must* be (it actually can be elsewhere, but this is the target)
  camera: { x: 0.0, y: 0.0, shaking: 0, shakingMax: 0 },
  // the PIXI app
  app: undefined,
  // container for all in-game objects, for camera simulation
  world: undefined
}
