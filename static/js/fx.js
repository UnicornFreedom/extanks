'use strict';

// most of special effects can be and must be reused for efficiency
// (ready to be reused effects will be hidden)

let FX = {
  instances: {
    explosions: []
  },
  update: function(delta) {
    Object.values(FX.instances).forEach(seq => {
      seq.forEach(it => it.update(delta));
    });
  }
};

class Effect {
  constructor(life) {
    this.lifeMax = life;
  }

  reset() {
    this.life = this.lifeMax;
    this.setVisible(true);
  }

  isAlive() { return this.life > 0.0; }

  isVisible() { return this.visible; }
  setVisible(value) { this.visible = value; }

  update(delta) {
    if (this.visible) {
      this.life -= delta;
      if (!this.isAlive()) this.setVisible(false);
    }
  }
}

class Explosion extends Effect {
  static create(x, y) {
    let exp = FX.instances.explosions.find(it => !it.isVisible());
    if (exp != undefined) {
      exp.reset(x, y);
    } else {
      exp = new Explosion(x, y);
      FX.instances.explosions.push(exp);
    }
  }

  constructor(x, y) {
    super(30); // life

    this.wave = sprite("image-shockwave");
    this.wave.anchor.set(0.5);
    this.wave.zIndex = Z_GROUND;
    STATE.world.addChild(this.wave);

    this.flash = sprite("image-flash");
    this.flash.anchor.set(0.5);
    this.flash.blendMode = PIXI.BLEND_MODES.ADD;
    STATE.world.addChild(this.flash);

    this.reset(x, y);
  }

  reset(x, y) {
    super.reset();
    this.setPosition(x, y);
  }

  setPosition(x, y) {
    this.wave.position.set(x, y);
    this.flash.position.set(x, y);
  }

  setVisible(value) {
    super.setVisible(value);
    this.wave.visible = value;
    this.flash.visible = value;
  }

  update(delta) {
    super.update(delta);
    if (this.visible) {
      let percent = 1.0 - (this.life / this.lifeMax);
      this.wave.scale.set(EasingFunctions.easeOutQuad(percent) * 1.5);
      let sin = Math.sin(percent * PI);
      this.wave.alpha = sin;

      if (Math.random() < sin) {
        if (Math.random() < 0.8) {
          Particle.createFire(this.wave.x + Math.random() * 100 - 50, this.wave.y + Math.random() * 100 - 50);
        } else {
          Particle.createSmoke(this.wave.x + Math.random() * 120 - 60, this.wave.y + Math.random() * 120 - 60);
        }
      } else if (percent > 0.4) {
        if (Math.random() < 0.8) {
          let angle = Math.random() * PI2;
          let rad = Math.random() * 60 + 60;
          Particle.createSmoke(this.wave.x + Math.cos(angle) * rad, this.wave.y + Math.sin(angle) * rad);
        }
      }

      if (percent < 0.5) {
        this.flash.scale.set(Math.abs(Math.sin(percent * PI2 * 2)) * 1.5);
      }
      else {
        this.flash.scale.set((1.0 - EasingFunctions.easeInQuad(percent)) * 1.5);
      }
    }
  }

  dispose() {
    STATE.world.removeChild(this.wave);
    STATE.world.removeChild(this.flash);
  }
}

class Particle extends Effect {
  static create(x, y, texture, life, blendMode, rotationSpeed, scaleMin, scaleMax) {
    if (!FX.instances[texture]) FX.instances[texture] = [];
    let instance = FX.instances[texture].find(it => !it.isVisible());
    if (instance != undefined) {
      instance.reset(x, y);
    } else {
      instance = new Particle(x, y, texture, life, blendMode, rotationSpeed, scaleMin, scaleMax);
      FX.instances[texture].push(instance);
    }
  }

  static createFire(x, y) {
    Particle.create(
      x, y,
      "image-fire",
      Math.random() * 100.0 + 100.0,    // life
      PIXI.BLEND_MODES.ADD,
      (Math.random() - 0.5) / 10.0,     // rotation speed
      0.25, Math.random() * 0.25 + 0.5, // scale
    );
  }

  static createSmoke(x, y) {
    Particle.create(
      x, y,
      "image-smoke",
      Math.random() * 200.0 + 200.0,  // life
      PIXI.BLEND_MODES.NORMAL,
      0,                              // rotation speed
      0.3, Math.random() * 0.3 + 0.3, // scale
    );
  }

  constructor(x, y, texture, life, blendMode, rotationSpeed, scaleMin, scaleMax) {
    super(life);

    this.sprite = sprite(texture);
    this.sprite.anchor.set(0.5);
    this.sprite.zIndex = Z_AIR;
    this.sprite.blendMode = blendMode;
    STATE.world.addChild(this.sprite);

    this.rotationSpeed = rotationSpeed;
    this.scaleMin = scaleMin;
    this.scaleMax = scaleMax;

    this.reset(x, y);
  }

  reset(x, y) {
    super.reset();
    this.setPosition(x, y);
    this.sprite.rotation = Math.random() * PI2;
  }

  setPosition(x, y) {
    this.sprite.position.set(x, y);
  }

  setVisible(value) {
    super.setVisible(value);
    this.sprite.visible = value;
  }

  update(delta) {
    super.update(delta);
    if (this.visible) {
      let percent = 1.0 - (this.life / this.lifeMax);
      let sin = Math.sin(EasingFunctions.easeOutCubic(percent) * PI);
      this.sprite.alpha = sin;
      this.sprite.scale.set(this.scaleMin + EasingFunctions.easeOutCubic(percent * (this.scaleMax - this.scaleMin)));
      this.sprite.rotation += this.rotationSpeed * EasingFunctions.easeInCubic(percent);
    }
  }

  dispose() {
    STATE.world.removeChild(this.sprite);
  }
}
