'use strict';

// go go go!
// time to create chaos!

// init some graphics here
//--------------------------------------------------------------------------------------------------------------------//
PIXI.settings.SPRITE_MAX_TEXTURES = Math.min(PIXI.settings.SPRITE_MAX_TEXTURES , 16);

STATE.app = new PIXI.Application({
  width: 800,
  height: 600,
  antialias: true,
  transparent: true,
  autoDensity: true
});
STATE.app.renderer.view.style.position = "absolute";
STATE.app.renderer.view.style.display = "block";
STATE.app.renderer.resize(window.innerWidth, window.innerHeight);
document.body.appendChild(STATE.app.view);

window.addEventListener("resize", function(event) {
  STATE.app.renderer.resize(window.innerWidth, window.innerHeight);
});

STATE.world = new PIXI.Container();
STATE.world.sortableChildren = true;
STATE.app.stage.addChild(STATE.world);

// ingnite the game cycle (loadingHandler -> setupHandler -> loopHandler)
//--------------------------------------------------------------------------------------------------------------------//
console.log("Loading resources...");

LOADER
  .add("image-tank-body", "images/tank-body.png")
  .add("image-tank-tower", "images/tank-tower.png")
  .add("image-spawn", "images/watermark.png")
  .add("image-shockwave", "images/shockwave.png")
  .add("image-flash", "images/flash.png")
  .add("image-fire", "images/fire.png")
  .add("image-smoke", "images/smoke.png")
  .on("progress", loadingHandler)
  .load(setupHandler);

// called while loading resources
function loadingHandler(loader, resource) {
  console.log("> " + resource.url + " " + loader.progress + "%");
}

// called after the loader finishes its work
function setupHandler(loader, resources) {
  console.log("Done!");
  console.log("Game objects initialization...")

  let spawn = sprite("image-spawn");
  spawn.anchor.set(0.5);
  spawn.position.set(STATE.app.view.width / 2, STATE.app.view.height / 2);
  STATE.world.addChild(spawn);

  STATE.tank = new Tank(STATE.app.view.width / 2, STATE.app.view.height / 2);
  STATE.tank.setBodyRotation(PI);
  STATE.tank.setTowerRotation(PI);

  console.log("Done!");
  console.log("Ready... Steady... Go!")

  // run game loop
  STATE.loaded = true;
  STATE.app.ticker.add(delta => loopHandler(delta));
}

// called by Pixi's ticker at 60 FPS rate
function loopHandler(delta) {
  // update game objects
  STATE.tank.update(delta);
  FX.update(delta);

  // move the camera after tank
  STATE.camera.x = STATE.tank.getX();
  STATE.camera.y = STATE.tank.getY();

  // update camera
  STATE.world.x = -STATE.camera.x + STATE.app.view.width / 2;
  STATE.world.y = -STATE.camera.y + STATE.app.view.height / 2;

  if (STATE.camera.shaking > 0) {
    STATE.camera.shaking--;
    let fade = STATE.camera.shaking / STATE.camera.shakingMax;
    let max = 4.0 * fade;
    STATE.world.x += Math.random() * max - max / 2.0;
    STATE.world.y += Math.random() * max - max / 2.0;
  }
}

// keyboard input handler
document.addEventListener('keydown', function(event) {
  if (STATE.loaded) {
    if (event.keyCode == 87) {
      STATE.tank.setAccelerator(true);
    } else if (event.keyCode == 83) {
      STATE.tank.setReverse(true);
    } else if (event.keyCode == 65) {
      STATE.tank.setTurningLeft(true);
    } else if (event.keyCode == 68) {
      STATE.tank.setTurningRight(true);
    }
  }
});

document.addEventListener('keyup', function(event) {
  if (STATE.loaded) {
    if (event.keyCode == 87) {
      STATE.tank.setAccelerator(false);
    } else if (event.keyCode == 83) {
      STATE.tank.setReverse(false);
    } else if (event.keyCode == 65) {
      STATE.tank.setTurningLeft(false);
    } else if (event.keyCode == 68) {
      STATE.tank.setTurningRight(false);
    }
  }
});

// mouse input handler
document.addEventListener('mousemove', function(event) {
  if (STATE.loaded) {
    STATE.tank.setTowerTarget(event.clientX + STATE.camera.x - STATE.app.view.width / 2, event.clientY + STATE.camera.y - STATE.app.view.height / 2);
  }
});

document.addEventListener('click', function(event) {
  if (STATE.camera.shaking == 0) {
    STATE.camera.shaking = Math.floor(Math.random() * 10.0 + 5.0);
    STATE.camera.shakingMax = STATE.camera.shaking;
  }
  if (STATE.loaded) {
    Explosion.create(event.clientX + STATE.camera.x - STATE.app.view.width / 2, event.clientY + STATE.camera.y - STATE.app.view.height / 2);
  }
});
